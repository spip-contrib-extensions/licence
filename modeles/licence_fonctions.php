<?php

/**
 * Plugin Licence
 * (c) 2007-2013 fanouch
 * Distribue sous licence GPL
 *
 * Inclusion des licences utiles pour le squelettes modeles/licence.html
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/licence');
