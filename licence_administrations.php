<?php

/**
 * Plugin Licence
 *
 * (c) 2007-2014 fanouch
 * Distribue sous licence GPL
 *
 * Modification des tables
 *
 * @package SPIP\Licence\Administration
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation ou mise à jour du plugin
 *
 * Ajoute un champ id_licence sur les tables spip_articles et spip_documents
 *
 * @param string $nom_meta_base_version
 * 		Le nom de la meta d'installation
 * @param float $version_cible
 * 		La version vers laquelle installer
 * @return void
 */
function licence_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [
		['maj_tables',['spip_articles','spip_documents']]
	];

	$maj['0.2.0'] = ['maj_tables',['spip_documents']];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Désinstallation du plugin
 *
 * On supprime :
 * -* La meta de configuration
 * -* La meta d'installation
 *
 * On laisse :
 * -* Les nouveaux champs sur les tables spip_documents et spip_articles
 *
 * @param string $nom_meta_base_version
 * 		Le nom de la meta d'installation
 * @return void
 */
function licence_vider_tables($nom_meta_base_version) {
	effacer_meta('licence');
	effacer_meta($nom_meta_base_version);
}
