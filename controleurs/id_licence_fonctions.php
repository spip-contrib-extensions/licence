<?php

/**
 * Plugin Licence
 * (c) 2007-2013 fanouch
 * Distribue sous licence GPL
 *
 * Inclusion des licences utiles pour le squelettes controleurs/id_licence.html
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/licence');
