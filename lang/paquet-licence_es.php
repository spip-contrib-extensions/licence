<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-licence?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'licence_description' => 'Permite unir una licencia de uso a un artículo o a un documento',
	'licence_nom' => 'Licencia',
	'licence_slogan' => 'Una Licencia para artículos y documentos'
);
