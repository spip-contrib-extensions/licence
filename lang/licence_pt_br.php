<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/licence?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Configuração do plugin licença',
	'cfg_descr_licence' => 'Licença para seus artigos',
	'cfg_lbl_licence_defaut' => 'Licença padrão',
	'cfg_titre_licence' => 'Licença',

	// D
	'description_art_libre' => 'Licença Arte Livre',
	'description_autre' => 'Outra licença',
	'description_cc0' => 'Creative Commons - Conteúdo livre de todo direito',
	'description_cc_by' => 'Creative Commons - Atribuição',
	'description_cc_by_nc' => 'Creative Commons - Atribuição - Sem utilização comercial ',
	'description_cc_by_nc_nd' => 'Creative Commons - Atribuição - Sem utilização comercial - Sem modificação',
	'description_cc_by_nc_sa' => 'Creative Commons - Atribuição - Compartilhamento nas mesmas condições',
	'description_cc_by_nd' => 'Creative Commons - Atribuição - Sem modificação ',
	'description_cc_by_sa' => 'Creative Commons - Atribuição - Compartilhamento nas mesmas condições',
	'description_copyright' => '© copyright autor do artigo',
	'description_gfdl' => 'Licença de documentação livre GNU',
	'description_gpl' => 'Licença GNU/GPL',
	'description_publicdomain' => 'Domínio público',
	'description_wtfpl' => 'Licença Pública Não Estou Nem Aí',

	// L
	'label_select_licence' => 'Escolha uma licença',
	'lien_art_libre' => 'http://artlibre.org',
	'lien_cc0' => 'https://br.creativecommons.org/licencas/',
	'lien_cc_by' => 'https://br.creativecommons.org/licencas/',
	'lien_cc_by_nc' => 'https://br.creativecommons.org/licencas/',
	'lien_cc_by_nc_nd' => 'https://br.creativecommons.org/licencas/',
	'lien_cc_by_nc_sa' => 'https://br.creativecommons.org/licencas/',
	'lien_cc_by_nd' => 'https://br.creativecommons.org/licencas/',
	'lien_cc_by_sa' => 'https://br.creativecommons.org/licencas/',
	'lien_gfdl' => 'https://www.gnu.org/licenses/fdl.html',
	'lien_gpl' => 'https://www.gnu.org/copyleft/gpl.html',
	'lien_publicdomain' => 'https://br.creativecommons.org/licencas/',
	'lien_whfpl' => 'http://sam.zoy.org/lprab/',

	// N
	'noisette_cacher_defaut' => 'Ocultar quando a licença não for especificada?',
	'noisette_hauteur_logo' => 'Altura máxima do logo (em pixels):',
	'noisette_largeur_logo' => 'Largura máxima do logo (em pixels):',
	'noisette_lien' => 'Exibir um link para a descrição da licença?',
	'noisette_logo' => 'Exibir o logo da licença?',
	'noisette_nom_licence' => 'Exibir o logo da licença?',

	// S
	'sans_licence' => 'Sem licença específica (direitos padrão)',

	// T
	'titre_art_libre' => 'LAL',
	'titre_autre' => 'outra',
	'titre_cc0' => 'CC0',
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gfdl' => 'GNU FDL',
	'titre_gpl' => 'GNU GPL',
	'titre_publicdomain' => 'Domínio público',
	'titre_wtfpl' => 'LPRAB'
);
