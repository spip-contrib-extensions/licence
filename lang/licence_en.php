<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/licence?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'License plugin configuration',
	'cfg_descr_licence' => 'Licenses for your articles',
	'cfg_lbl_licence_defaut' => 'Default license',
	'cfg_titre_licence' => 'License',

	// D
	'description_art_libre' => 'Free Art License',
	'description_autre' => 'Other license',
	'description_cc0' => 'Creative Common - Public Domain Dedication',
	'description_cc_by' => 'Creative Commons - Attribution',
	'description_cc_by_nc' => 'Creative Commons - Attribution Non-Commercial',
	'description_cc_by_nc_nd' => 'Creative Commons - Attribution Non-Commercial No Derivatives',
	'description_cc_by_nc_sa' => 'Creative Commons - Attribution Non-Commercial Share Alike',
	'description_cc_by_nd' => 'Creative Commons - Attribution No Derivatives',
	'description_cc_by_sa' => 'Creative Commons - Attribution Share Alike',
	'description_copyright' => '© copyright the article’s author',
	'description_gfdl' => 'GNU Free Documentation License',
	'description_gpl' => 'GNU/GPL License',
	'description_publicdomain' => 'Public domain',
	'description_wtfpl' => 'Do What The Fuck You Want To Public License ',

	// L
	'label_select_licence' => 'Choose a license',
	'lien_art_libre' => 'http://artlibre.org/licence/lal/en',
	'lien_cc0' => 'https://creativecommons.org/publicdomain/zero/1.0/',
	'lien_cc_by' => 'https://creativecommons.org/licenses/by/3.0/deed.en',
	'lien_cc_by_nc' => 'https://creativecommons.org/licenses/by-nc/3.0/deed.en',
	'lien_cc_by_nc_nd' => 'https://creativecommons.org/licenses/by-nc-nd/3.0/deed.en',
	'lien_cc_by_nc_sa' => 'https://creativecommons.org/licenses/by-nc-sa/3.0/deed.en',
	'lien_cc_by_nd' => 'https://creativecommons.org/licenses/by-nd/3.0/deed.en',
	'lien_cc_by_sa' => 'https://creativecommons.org/licenses/by-sa/3.0/deed.en',
	'lien_gfdl' => 'https://www.gnu.org/licenses/fdl.html',
	'lien_gpl' => 'https://www.gnu.org/copyleft/gpl.html',
	'lien_publicdomain' => 'https://creativecommons.org/publicdomain/mark/1.0/deed.en',
	'lien_whfpl' => 'http://sam.zoy.org/wtfpl/',

	// N
	'noisette_cacher_defaut' => 'Hide when the license is not specified?',
	'noisette_hauteur_logo' => 'Maximum height of the logo (in pixels):',
	'noisette_largeur_logo' => 'Maximum width of the logo (in pixels):',
	'noisette_lien' => 'Display the link to the license description?',
	'noisette_logo' => 'Display the license logo?',
	'noisette_nom_licence' => 'Display the license name?',

	// S
	'sans_licence' => 'No specific license (default rights)',

	// T
	'titre_art_libre' => 'FAL',
	'titre_autre' => 'other',
	'titre_cc0' => 'CC0',
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gfdl' => 'GNU FDL',
	'titre_gpl' => 'GNU GPL',
	'titre_publicdomain' => 'Public domain',
	'titre_wtfpl' => 'WTFPL'
);
