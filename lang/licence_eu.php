<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/licence?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Lizentzia plugin-aren konfigurazioa',
	'cfg_descr_licence' => 'Lizentzia batzuk zuen artikuluentzat',
	'cfg_lbl_licence_defaut' => 'Lehenetsitako lizentzia',
	'cfg_titre_licence' => 'Lizentzia',

	// D
	'description_art_libre' => 'Art Libre lizentzia',
	'description_cc0' => 'Creative Commons - Edukia eskubidez libre',
	'description_cc_by' => 'Creative Commons - Aitortu',
	'description_cc_by_nc' => 'Creative Commons - Aitortu - EzKomertziala',
	'description_cc_by_nc_nd' => 'Creative Commons - Aitortu - Ezkomertziala - LanEratorririkgabe',
	'description_cc_by_nc_sa' => 'Creative Commons - Aitortu - EzKomertziala - PartekatuBerdin',
	'description_cc_by_nd' => 'Creative Commons - Aitortu - LanEratorririkGabe',
	'description_cc_by_sa' => 'Creative Commons - Aitortu - PartekatuBerdin',
	'description_copyright' => '© copyright artikuluaren egilea',
	'description_gfdl' => 'GNU Dokumentazio Librearen Lizentzia',
	'description_gpl' => 'GNU/GPL GNU Lizentzia Publiko orokorra',
	'description_wtfpl' => 'Lizentzia Publikoa Bost Axola',

	// L
	'label_select_licence' => 'Lizentzia bat aukeratu',
	'lien_art_libre' => 'http://artlibre.org/licence/lal', # MODIF
	'lien_cc0' => 'http://creativecommons.org/publicdomain/zero/1.0/deed.fr', # MODIF
	'lien_cc_by' => 'http://creativecommons.org/licenses/by/3.0/deed.fr', # MODIF
	'lien_cc_by_nc' => 'http://creativecommons.org/licenses/by-nc/3.0/deed.fr', # MODIF
	'lien_cc_by_nc_nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/deed.fr', # MODIF
	'lien_cc_by_nc_sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/deed.fr', # MODIF
	'lien_cc_by_nd' => 'http://creativecommons.org/licenses/by-nd/3.0/deed.fr', # MODIF
	'lien_cc_by_sa' => 'http://creativecommons.org/licenses/by-sa/3.0/deed.fr', # MODIF
	'lien_gfdl' => 'http://www.gnu.org/licenses/fdl.html', # MODIF
	'lien_gpl' => 'http://www.gnu.org/copyleft/gpl.html', # MODIF
	'lien_whfpl' => 'http://www.wtfpl.net/',

	// N
	'noisette_cacher_defaut' => 'Ezkutatu lizentzia ez dagoelarik zehaztua?',
	'noisette_hauteur_logo' => 'Logotipoaren gehiengo altuera',
	'noisette_largeur_logo' => 'Logotipoaren gehiengo zabalera (pixeletan) :',
	'noisette_lien' => 'Lizentziaren deskribapenari buruzko lotura ezarri?',
	'noisette_logo' => 'Logotipoaren lizentzia bistaratu?',
	'noisette_nom_licence' => 'Lizentziaren izena bistaratu?',

	// S
	'sans_licence' => 'Lizentziarik gabe', # MODIF

	// T
	'titre_art_libre' => 'Art Libre',
	'titre_cc0' => 'CC0',
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gfdl' => 'GNU FDL',
	'titre_gpl' => 'GNU GPL',
	'titre_wtfpl' => 'WTFPL'
);
