<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/licence.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Configuration du plugin licence',
	'cfg_descr_licence' => 'Des licences pour vos articles',
	'cfg_lbl_licence_defaut' => 'Licence par défaut',
	'cfg_titre_licence' => 'Licence',

	// D
	'description_art_libre' => 'Licence Art Libre',
	'description_autre' => 'Autre licence',
	'description_cc0' => 'Creative Commons - Contenu libre de tout droit',
	'description_cc_by' => 'Creative Commons - Attribution',
	'description_cc_by_nc' => 'Creative Commons - Attribution - Pas d’Utilisation Commerciale ',
	'description_cc_by_nc_nd' => 'Creative Commons - Attribution - Pas d’Utilisation Commerciale - Pas de Modification',
	'description_cc_by_nc_sa' => 'Creative Commons - Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions',
	'description_cc_by_nd' => 'Creative Commons - Attribution - Pas de Modification ',
	'description_cc_by_sa' => 'Creative Commons - Attribution - Partage dans les Mêmes Conditions',
	'description_copyright' => '© copyright auteur de l’article',
	'description_gfdl' => 'Licence de documentation libre GNU',
	'description_gpl' => 'Licence GNU/GPL',
	'description_publicdomain' => 'Domaine public',
	'description_wtfpl' => 'Licence Publique Rien À Branler',

	// L
	'label_select_licence' => 'Choisissez une licence',
	'lien_art_libre' => 'http://artlibre.org',
	'lien_cc0' => 'https://creativecommons.org/publicdomain/zero/1.0/deed.fr',
	'lien_cc_by' => 'https://creativecommons.org/licenses/by/3.0/deed.fr',
	'lien_cc_by_nc' => 'https://creativecommons.org/licenses/by-nc/3.0/deed.fr',
	'lien_cc_by_nc_nd' => 'https://creativecommons.org/licenses/by-nc-nd/3.0/deed.fr',
	'lien_cc_by_nc_sa' => 'https://creativecommons.org/licenses/by-nc-sa/3.0/deed.fr',
	'lien_cc_by_nd' => 'https://creativecommons.org/licenses/by-nd/3.0/deed.fr',
	'lien_cc_by_sa' => 'https://creativecommons.org/licenses/by-sa/3.0/deed.fr',
	'lien_gfdl' => 'https://www.gnu.org/licenses/fdl.html',
	'lien_gpl' => 'https://www.gnu.org/copyleft/gpl.html',
	'lien_publicdomain' => 'https://creativecommons.org/publicdomain/mark/1.0/deed.fr',
	'lien_whfpl' => 'http://sam.zoy.org/lprab/',

	// N
	'noisette_cacher_defaut' => 'Cacher lorsque la licence n’est pas spécifiée ?',
	'noisette_hauteur_logo' => 'Hauteur maximale du logo (en pixels) :',
	'noisette_largeur_logo' => 'Largeur maximale du logo (en pixels) :',
	'noisette_lien' => 'Afficher le lien vers la description de la licence ?',
	'noisette_logo' => 'Afficher le logo de la licence ?',
	'noisette_nom_licence' => 'Afficher le nom de la licence ?',

	// S
	'sans_licence' => 'Pas de licence spécifique (droits par défaut)',

	// T
	'titre_art_libre' => 'LAL',
	'titre_autre' => 'autre',
	'titre_cc0' => 'CC0',
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gfdl' => 'GNU FDL',
	'titre_gpl' => 'GNU GPL',
	'titre_publicdomain' => 'Domaine public',
	'titre_wtfpl' => 'LPRAB'
);
