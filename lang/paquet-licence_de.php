<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-licence?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'licence_description' => 'Artikel und Dokumente können individuelle Nutzungslizenzen zugeordnet werden.',
	'licence_nom' => 'Lizenz',
	'licence_slogan' => 'Lizenzen für Artikel und Dokumente'
);
