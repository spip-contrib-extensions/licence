<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/licence?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Configurazione del plugin licenza',
	'cfg_descr_licence' => 'Licenze per i vostri articoli',
	'cfg_lbl_licence_defaut' => 'Licenza di default',
	'cfg_titre_licence' => 'Licenza',

	// D
	'description_art_libre' => 'Licenza Arte Libera', # MODIF
	'description_cc_by' => 'Creative Commons - Attribuzione', # MODIF
	'description_cc_by_nc' => 'Creative Commons - Attribuzione Non uso commerciale', # MODIF
	'description_cc_by_nc_nd' => 'Creative Commons - Attribuzione Non uso commerciale Non opere derivate', # MODIF
	'description_cc_by_nc_sa' => 'Creative Commons - Attribuzione Non uso commerciale Condividi allo stesso modo', # MODIF
	'description_cc_by_nd' => 'Creative Commons - Attribuzione Non opere derivate', # MODIF
	'description_cc_by_sa' => 'Creative Commons - Attribuzione Condividi allo stesso modo', # MODIF
	'description_copyright' => '© copyright autore dell’articolo',
	'description_gpl' => 'licenza GPL', # MODIF

	// L
	'label_select_licence' => 'Scegliete una licenza',
	'lien_art_libre' => 'http://artlibre.org/', # MODIF
	'lien_cc_by' => 'http://creativecommons.org/licenses/by/3.0/deed.it', # MODIF
	'lien_cc_by_nc' => 'http://creativecommons.org/licenses/by-nc/3.0/deed.it', # MODIF
	'lien_cc_by_nc_nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/deed.it', # MODIF
	'lien_cc_by_nc_sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/deed.it', # MODIF
	'lien_cc_by_nd' => 'http://creativecommons.org/licenses/by-nd/3.0/deed.it', # MODIF
	'lien_cc_by_sa' => 'http://creativecommons.org/licenses/by-sa/3.0/deed.it', # MODIF
	'lien_gpl' => 'http://www.gnu.org/copyleft/gpl.html', # MODIF

	// S
	'sans_licence' => 'Senza licenza', # MODIF

	// T
	'titre_art_libre' => 'LAL',
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gpl' => 'Gnu GPL' # MODIF
);
