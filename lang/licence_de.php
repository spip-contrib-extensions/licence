<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/licence?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Konfiguration des Plugins Lizenz',
	'cfg_descr_licence' => 'Lizenzen für ihre Artikel',
	'cfg_lbl_licence_defaut' => 'Standardlizenz',
	'cfg_titre_licence' => 'Lizenz',

	// D
	'description_art_libre' => 'Lizenz Freie Kunst',
	'description_autre' => 'Andere Lizenz',
	'description_cc0' => 'Creative Common - Kein Urheberrechtsschutz',
	'description_cc_by' => 'Creative Commons - Namensnennung',
	'description_cc_by_nc' => 'Creative Commons - Namensnennung, keine kommerzielle Nutzung',
	'description_cc_by_nc_nd' => 'Creative Commons -  Namensnennung, keine kommerzielle Nutzung, keine Bearbeitung',
	'description_cc_by_nc_sa' => 'Creative Commons - Namensnennung, keine kommerzielle Nutzung, Weitergabe unter gleichen Bedingungen',
	'description_cc_by_nd' => 'Creative Commons - Namensnennung, keine Bearbeitung',
	'description_cc_by_sa' => 'Creative Commons - Namensnennung, Weitergabe unter gleichen Bedingungen',
	'description_copyright' => '© Copyright liegt beim Autor des Artikels',
	'description_gfdl' => 'GNU Freie Dokumentationslizenz',
	'description_gpl' => 'GPL-Lizenz',
	'description_publicdomain' => 'Gemeinfrei',
	'description_wtfpl' => 'Öffentliche Scheisst-der-Hund-drauf Lizenz',

	// L
	'label_select_licence' => 'Wählen sie eine Lizenz',
	'lien_art_libre' => 'http://artlibre.org/licence/lal/de/',
	'lien_cc0' => 'http://creativecommons.org/publicdomain/zero/1.0/deed.de',
	'lien_cc_by' => 'http://creativecommons.org/licenses/by/3.0/deed.de',
	'lien_cc_by_nc' => 'http://creativecommons.org/licenses/by-nc/3.0/deed.de',
	'lien_cc_by_nc_nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/deed.de',
	'lien_cc_by_nc_sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/deed.de',
	'lien_cc_by_nd' => 'http://creativecommons.org/licenses/by-nd/3.0/deed.de',
	'lien_cc_by_sa' => 'http://creativecommons.org/licenses/by-sa/3.0/deed.de',
	'lien_gfdl' => 'http://www.gnu.org/licenses/fdl.html',
	'lien_gpl' => 'http://www.gnu.org/copyleft/gpl.html',
	'lien_publicdomain' => 'https://creativecommons.org/publicdomain/mark/1.0/deed.de',
	'lien_whfpl' => 'http://sam.zoy.org/lprab/',

	// N
	'noisette_cacher_defaut' => 'Nichts anzeigen wenn keine spezielleLizenz angegeben wurde ?',
	'noisette_hauteur_logo' => 'Maximale Höhe des Logos (in Pixel):',
	'noisette_largeur_logo' => 'Maximale Breite des Logos (in Pixel):',
	'noisette_lien' => 'Link zur Breschreibung der Lizenz anzeigen?',
	'noisette_logo' => 'Logo der Lizenz anzeigen?',
	'noisette_nom_licence' => 'Bezeichnung der Lizenz anzeigen?',

	// S
	'sans_licence' => 'Es gelten die Regeln des Urheberrechts',

	// T
	'titre_art_libre' => 'LFK',
	'titre_autre' => 'andere',
	'titre_cc0' => 'CC0',
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gfdl' => 'GNU FDL',
	'titre_gpl' => 'GNU GPL',
	'titre_publicdomain' => 'Gemeinfrei',
	'titre_wtfpl' => 'LPRAB'
);
