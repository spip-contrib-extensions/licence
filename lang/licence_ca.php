<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/licence?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_boite_licence' => 'Configuració del connector llicència',
	'cfg_descr_licence' => 'Llicències pels vostres articles',
	'cfg_lbl_licence_defaut' => 'Llicència per defecte',
	'cfg_titre_licence' => 'Llicència',

	// D
	'description_art_libre' => 'Llicència Art lliure', # MODIF
	'description_cc_by' => 'Creative Commons - Reconeixement', # MODIF
	'description_cc_by_nc' => 'Creative Commons - No comercial', # MODIF
	'description_cc_by_nc_nd' => 'Creative Commons - No comercial No es pot modificar', # MODIF
	'description_cc_by_nc_sa' => 'Creative Commons - Reconeixement No ús comercial Compartir amb les mateixes condicions inicials i amb llicència idèntica', # MODIF
	'description_cc_by_nd' => 'Creative Commons - Reconeixement sense modificació', # MODIF
	'description_cc_by_sa' => 'Creative Commons - Reconeixement Compartir amb les mateixes condicions inicials i llicència idèntica', # MODIF
	'description_copyright' => '© copyright autor de l’article',
	'description_gpl' => 'llicència GPL', # MODIF

	// L
	'label_select_licence' => 'Escolliu una llicència',
	'lien_art_libre' => 'http://artlibre.org/', # MODIF
	'lien_cc_by' => 'http://creativecommons.org/licenses/by/3.0/deed.ca', # MODIF
	'lien_cc_by_nc' => 'http://creativecommons.org/licenses/by-nc/3.0/deed.ca', # MODIF
	'lien_cc_by_nc_nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/deed.ca', # MODIF
	'lien_cc_by_nc_sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/deed.ca', # MODIF
	'lien_cc_by_nd' => 'http://creativecommons.org/licenses/by-nd/3.0/deed.ca', # MODIF
	'lien_cc_by_sa' => 'http://creativecommons.org/licenses/by-sa/3.0/deed.ca', # MODIF
	'lien_gpl' => 'http://www.gnu.org/copyleft/gpl.html', # MODIF

	// S
	'sans_licence' => 'Sense llicència', # MODIF

	// T
	'titre_art_libre' => 'LAL',
	'titre_cc_by' => 'CC by',
	'titre_cc_by_nc' => 'CC by-nc',
	'titre_cc_by_nc_nd' => 'CC by-nc-nd',
	'titre_cc_by_nc_sa' => 'CC by-nc-sa',
	'titre_cc_by_nd' => 'CC by-nd',
	'titre_cc_by_sa' => 'CC by-sa',
	'titre_copyright' => 'Copyright',
	'titre_gpl' => 'Gnu GPL' # MODIF
);
